FROM ubuntu:latest
LABEL maitainer="actualizacion@tecnologica"
LABEL version="2"
LABEL description="Practica Dockerfile con wordpress y mysql"

ARG DEBIAN_FRONTEND=noninteractive

RUN apt update && apt install -y wget apache2 mysql-server php libapache2-mod-php php-mysql php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap php-intl php-zip && apt autoremove && apt clean



ENV APPSERVERNAME example.com
ENV APPALIAS www.example.com
ENV MYSQL_USER wpuser
ENV MYSQL_USER_PASSWORD 1234
ENV MYSQL_DB_NAME wordpress


# Creamos el directorio 'app' en el raiz
RUN mkdir -p /app
# Copiamos el fichero de vhost a 'sites-available'
COPY default.conf /etc/apache2/sites-available
# Copiamos el directorio 'apache2' de 'etc' hacia '/app/apache2'
# Si hay un punto de montaje hacia el directorio 'apache2'
# El entrypoint al encontrar vacio al directorio va a traer los ficheros 
# correspondientes de '/app/apache2'
RUN cp -r /etc/apache2 /app/apache2



VOLUME ["/var/www/html", "/var/lib/mysql", "/etc/apache2"]

COPY entrypoint.sh /
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]








